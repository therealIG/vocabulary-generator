#!/bin/bash

mkdir -p $2
cat $1 | xargs -n 1 -P4 wget --quiet -T 3 --tries=2 -P "$2" 
rm -f $1
