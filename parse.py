from selenium import webdriver
from bs4 import BeautifulSoup
from yandex_translate import YandexTranslate
import time
import subprocess
import os
import pandas as pd
import sys

_author_ = "Dmitry Shuklinov, Ivan Goncharov"

chrome_options = webdriver.ChromeOptions()
chrome_options.set_headless(headless=True)
chrome_options.add_experimental_option(
    "excludeSwitches", ["ignore-certificate-errors"])
check_driver_version = 1
driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')
translate = YandexTranslate('trnsl.1.1.20181025T185056Z.a4588f3903ea7888.144d3275d793ff20f9cd0d3bab017a57bc89ae16')


def read_file(path):

    """
    Reads .txt file
    :param path: path to file
    :return: string
    """
    file = open(path, 'r')
    return file.read()


def input_text(path):

    """
    Inputs content of file to vocabulary.com
    :param path: path to file
    """
    driver.get('https://www.vocabulary.com/lists/vocabgrabber')
    textarea = driver.find_element_by_tag_name('textarea')
    content = read_file(path)
    textarea.send_keys(content)
    driver.find_elements_by_tag_name('button')[1].click()


def get_list(count):

    """
    Gets vocabulary words from vocabulary.com
    :param count: needed count of words
    :return: list
    """
    time.sleep(3)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    list_div = soup.find('div', class_='list')
    vocabular = [word.text for word in list_div.find_all('td', class_='word')]
    return vocabular[:count]


def create_url_file(list):

    """
    Creates urls for vocabulary words
    :param list:
    """
    url_list = []
    for word in list:
        url = 'https://en.oxforddictionaries.com/definition/' + word
        url_list.append(url)
    with open('urls.txt', 'w') as file:
        for url in url_list:
            file.write("%s\n" % url)


def create_html_dir():

    """
    Parallel download urls from create_url_file with bash script
    Deletes urls.txt file!
    """
    bash_script = './download_html.sh ' + os.getcwd() + '/urls.txt'
    bash_script = bash_script + ' ' + os.getcwd() +'/htmls'
    subprocess.call(bash_script, shell=True)


def create_table(word_list):

    """
    Creates dictionary of dictionaries with all data for word
    :param word_list: list of words
    :return: dictionary
    """
    table = {}
    i = 1
    for word in word_list:
        f = open('./htmls/' + word, 'r')
        soup = BeautifulSoup(f.read(), 'lxml')
        f.close()
        try:
            word_type = soup.find('span', class_='pos').text
            pronunciation = soup.find('span', class_='phoneticspelling').text[1:-1]
            example = soup.find('li', class_='ex').text
            example = example[2:example.find(';')]
            meaning = soup.find('span', class_='ind').text
            meaning = meaning[:meaning.find(';')]
            table.update({i: {'WORD': [word],
                            'GRAMMAR INFO': [word_type],
                            'PRONUNCIATION': [pronunciation],
                            'MEANING': [meaning],
                            'EXAMPLE': [example],
                            'TRANSLATE': [translate.translate(word, 'en-ru')['text'][0]]}})
            i += 1
        except:
            pass
    return table


def create_dataframe(dicts):

    """
    Creates dataframe with all data
    :param dicts: Dictionary of dictionaries
    :return: Pandas Dataframe
    """
    data = pd.DataFrame()
    for index, value in dicts.items():
        app_data = pd.DataFrame.from_dict(value)
        data = data.append(app_data)
    return data

def clear_directory():

    """
    Removes unnecessary files from directory
    """
    bash = 'rm -r ' + os.getcwd() +'/htmls'
    subprocess.call(bash, shell=True)


def main():

    path = sys.argv[1]
    count = int(sys.argv[2])
    input_text(path)
    words_list = get_list(count)
    create_url_file(words_list)
    create_html_dir()
    dicts = create_table(words_list)
    data = create_dataframe(dicts)
    data.to_csv('vocabulary.csv', index = False)
    print('VOCABULARY SUCSESSFULLY CREATED')
    clear_directory()
    pass


if __name__ == "__main__":
    main()