# Vocabulary generator
Generates vocabulary for any English text   
Reads .txt file and generates a vocabulary .csv file of the form
WORD/GRAMMAR INFO/PRONUNCIATION/MEANING/EXAMPLE/TRANSLATE    

## Essential software   
Hereafter it is assumed that you are using Ubuntu OS
1. The first thing to do is to install the required packages.   

This can be done with:   

```bash
pip3 install -r requirements.txt yandex.translate  
```
executed from terminal.   

2. Also, [Google Chrome must be installed](https://www.google.com/intl/ru_ALL/chrome/)

## Using
You should make the script executable with:     
```bash
chmod +x parse.py    
```
Finally you are able to run it with:   
```bash
python3 parse.py */filename/* */words_cnt/*
```
The file is assumed to have an extension .txt and is located in the same folder with the script   

## You are free to contact us:    

therealg4@protonmail.ch - Ivan Goncharov    

shuklinov.d@yandex.ru - Dmitry Shuklinov    

### P.S.

The speed depends on the quality of the Internet connection.   

WARNING: Script uses sources: [1](https://www.vocabulary.com/), [2](https://en.oxforddictionaries.com/definition/), [3](https://github.com/dveselov/python-yandex-translate)   

